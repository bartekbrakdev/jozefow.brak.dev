function Config(){};

//---

Config.PATH = '/';

Config.PATH_DATA = 'file.php?file=';

Config.SERVICE_ID = 0;

Config.PARENT_ID = 0;

Config.PAGE_ID = 0;

//---

Config.dir = {
	ADMIN:			'admin/',
	COMMON:			'common/',
	DATA:			'data/',
	JS:				'js/',
	PAGE:			'page/',
	DATA_LAYOUT:	'layout/'
};

//---

Config.file = {
	MAIN:		'index.php',
	VIEWER:		'file.php',
	NEWSLETTER: 'newsletter.php',
	PRINTER:	'print.php',
	RESIZER: 	'resizer.php',
	NOTIFIER: 	'notifier.php'
};

//---

Config.getPath = function(){
	return Config.PATH;
};

Config.getPathToCommon = function(){
	return Config.PATH + Config.dir.COMMON;
};

Config.getPathToAdmin = function(){
	return Config.PATH + Config.dir.ADMIN;
};

Config.getPathToData = function(isPHP){
	return isPHP ? Config.PATH_DATA : Config.PATH + Config.dir.DATA;
};

Config.getPathToJS = function(){
	return Config.PATH + Config.dir.JS;
};

Config.getPathToPage = function(){
	return Config.PATH + Config.dir.PAGE;
};
