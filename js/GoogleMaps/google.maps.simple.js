googleMapsSimple = {};

googleMapsSimple.mapContainter 		= 'mapSimpleBox';
googleMapsSimple.linkToInstitution  = '?str={str}&id={id}';
googleMapsSimple.map 				= null;
googleMapsSimple.infoWindow 		= null;
googleMapsSimple.Icon 				= '';
googleMapsSimple.X 					= 0;
googleMapsSimple.Y 					= 0;
googleMapsSimple.zoom 				= 13;
googleMapsSimple.iconWidth	 		= googleMapsConfig.icon.width;
googleMapsSimple.iconHeight  		= googleMapsConfig.icon.height;
googleMapsSimple.point  = {};
googleMapsSimple.labels = {
	  'more' 	: ''
};

//Main initialization
googleMapsSimple.init = function() {

	var mapOptions = {
			zoom: googleMapsSimple.zoom,
			center: new google.maps.LatLng(googleMapsSimple.X, googleMapsSimple.Y),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	googleMapsSimple.map = new google.maps.Map(document.getElementById(googleMapsSimple.mapContainter), mapOptions);
	googleMapsSimple.infoWindow = new google.maps.InfoWindow();

    googleMapsSimple.addMarker();

};
//Create marker on map
googleMapsSimple.addMarker = function() {

	var image = null;

	// Creating the icon
    if(googleMapsSimple.Icon != ''){
	    image = new google.maps.MarkerImage(
	    	'../data/mapIcons/' + googleMapsSimple.Icon,									//url
		    new google.maps.Size(googleMapsSimple.iconWidth, googleMapsSimple.iconHeight),	//size
		    new google.maps.Point(0,0),														//origin
		    new google.maps.Point(parseInt(googleMapsSimple.iconWidth / 2), 5)				//anchor
		);
    }

    //Creating marker
    var pointLatLng = new google.maps.LatLng(googleMapsSimple.X, googleMapsSimple.Y);
    var marker = new google.maps.Marker({
        position: 	pointLatLng,
        map: 		googleMapsSimple.map,
        icon: 		image
    });

    // Add a click event to a marker
    google.maps.event.addListener(marker, 'click', function() {
    	if (!googleMapsSimple.infoWindow) {
    		googleMapsSimple.infoWindow = new google.maps.InfoWindow();
    	}
    	var content = googleMapsSimple.setContent();
    	googleMapsSimple.infoWindow.setContent(content);
    	googleMapsSimple.infoWindow.open(googleMapsSimple.map, marker);
    });

    google.maps.event.trigger(marker, 'click');

};

//Create content for marker;
googleMapsSimple.setContent = function() {
		var content = '<div class="marker_title"><b>' + googleMapsSimple.point.name + '</b></div>';
		content += '<div>' + googleMapsSimple.point.address + '</div>';
		if(googleMapsSimple.point.phone != ''){
			content += '<div>tel.: ' + googleMapsSimple.point.phone + '</div>';
		}
		if(googleMapsSimple.point.fax != ''){
			content += '<div>fax: ' + googleMapsSimple.point.fax + '</div>';
		}
		if(googleMapsSimple.point.email != ''){
			content += '<div>e-mail: <a href="mailto:' + googleMapsSimple.point.email + '">' + googleMapsSimple.point.email + '</a></div>';
		}
		if(googleMapsSimple.point.link != ''){
			content += '<div>www: <a href="' + googleMapsSimple.point.link + '" target="_blank">' + googleMapsSimple.point.link + '</a></div>';
		}
		if(googleMapsSimple.point.pageId != '' && googleMapsSimple.point.institutionId != ''){
			var href = googleMapsSimple.linkToInstitution;
			href = href.replace('{str}', googleMapsSimple.point.pageId);
			href = href.replace('{id}', googleMapsSimple.point.institutionId);
		}
		content = '<div class="marker_window">' + content + '</div>';

 return content;
};