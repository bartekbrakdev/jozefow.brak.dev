googleMapsPoints = {};

googleMapsPoints.XMLFile 				= '';
googleMapsPoints.linesJSON 				= '';
googleMapsPoints.iconsPath 				= '';
googleMapsPoints.iconWidth				= googleMapsConfig.icon.width;
googleMapsPoints.iconHeight    			= googleMapsConfig.icon.height;
googleMapsPoints.layersCount 			= 0;
googleMapsPoints.layersAndLinesCount 	= 0;
googleMapsPoints.selectedLayerId 		= 0;
googleMapsPoints.checkboxLayerPrefix 	= 'layer';
googleMapsPoints.linkToInstitution 		= '{str}?id={id}';
googleMapsPoints.map 			  		= null;
googleMapsPoints.infoWindow 			= null;
googleMapsPoints.geo 			  		= null;
googleMapsPoints.isLinesLoading  		= null;
googleMapsPoints.markers				= Array();
googleMapsPoints.centerX 				= googleMapsConfig.position.X;
googleMapsPoints.centerY 				= googleMapsConfig.position.Y;
googleMapsPoints.zoom 					= googleMapsConfig.zoom;
googleMapsPoints.labels 				= {
											  'more' 	: ''
											, 'address' : ''
											, 'phone'	: ''
											, 'fax'		: ''
											, 'email'	: ''
											, 'www'		: ''
										};
//Initialization
googleMapsPoints.init = function(){

	var mapOptions = {
			zoom: googleMapsPoints.zoom,
			center: new google.maps.LatLng(googleMapsPoints.centerX, googleMapsPoints.centerY),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	googleMapsPoints.map = new google.maps.Map(document.getElementById("mapBox"), mapOptions);
	googleMapsPoints.infoWindow = new google.maps.InfoWindow();

	googleMapsPoints.loadMarkersFromXML();

};

//Loading markers from XML
googleMapsPoints.loadMarkersFromXML = function(){
	var attributes = ['id', 'name', 'description', 'address', 'phone', 'fax', 'email', 'link', 'icon', 'x', 'y', 'layerId', 'pageId', 'institutionId'];

	$.ajax({
	      type: 	"GET",
	      url: 		googleMapsPoints.XMLFile,
	      dataType: "xml",
	      success: function(data) {
			 xml = $(data);
			 xml.find("point").each(function(){
	       		 var v = {};
	           	 for(var j = 0; j < attributes.length; j++){
	           		v[attributes[j]] = $(this).attr(attributes[j]).replace(/&quot;/g,'"');
	           	 }
	   			 var id = $(this).attr('id');
	   			 var layerId = $(this).attr('layerId');
	   			 if((googleMapsPoints.selectedLayerId==0) || (googleMapsPoints.selectedLayerId==layerId)) {
	   	            googleMapsPoints.markers[id] = {
	   	            	  'layerId'	: layerId
	               		, 'marker'	: googleMapsPoints.createMarker(v)
	               		};
	   			 }
			 });
			 googleMapsPoints.showMarkersOnMap();			 			 document.getElementById('mapBox').style.backgroundImage = 'none';
	      }
	});
};

//Create marker
googleMapsPoints.createMarker = function(v){
	var image = null;

	// Creating the icon
    if(v.icon != ''){
	    image = new google.maps.MarkerImage(
		    googleMapsPoints.iconsPath + v.icon,											//url
		    new google.maps.Size(googleMapsPoints.iconWidth, googleMapsPoints.iconHeight),	//size
		    new google.maps.Point(0,0),														//origin
		    new google.maps.Point(parseInt(googleMapsPoints.iconWidth / 2), googleMapsPoints.iconHeight) //anchor
		);
    }

    //Creating marker
    var pointLatLng = new google.maps.LatLng(parseFloat(v.x), parseFloat(v.y));
    var marker = new google.maps.Marker({
        position: 	pointLatLng,
        //map: 		googleMapsPoints.map,
        icon: 		image,
        title: 		v.name
    });

    //Prepare content
    var content = '<div class="marker_title"><b>' + v.name + '</b></div>';
	if(parseInt(v.institutionId) == 0){
		if(v.description != ''){
			content += '<div>' + v.description + '</div>';
		}
		if(v.link != ''){
			content += '<div><a href="' + v.link + '" target="_blank">' + v.link + '</a></div>';
		}
	} else {
		content += '<div>' + v.address + '</div>';
		if(v.phone != ''){
			content += '<div>tel.: ' + v.phone + '</div>';
		}
		if(v.fax != ''){
			content += '<div>fax: ' + v.fax + '</div>';
		}
		if(v.email != ''){
			content += '<div>e-mail: <a href="mailto:' + v.email + '">' + v.email + '</a></div>';
		}
		if(v.link != ''){
			content += '<div>www: <a href="' + v.link + '" target="_blank">' + v.link + '</a></div>';
		}
		if(v.pageId != '' && v.institutionId != ''){
			var href = googleMapsPoints.linkToInstitution;
			href = href.replace('{str}', v.pageId);
			href = href.replace('{id}', v.institutionId);
			content += '<div style="margin: 5px 0 0 0;"><a href="' + href + '" target="_blank">'
			+ googleMapsPoints.labels.more + '</a></div>';
		}
	}
	content = '<div class="marker_window">' + content + '</div>';

    // Add a click event to a marker
    google.maps.event.addListener(marker, 'click', function() {
    	if (!googleMapsPoints.infoWindow) {
    		googleMapsPoints.infoWindow = new google.maps.InfoWindow();
    	}

    	googleMapsPoints.infoWindow.setContent(content);
    	googleMapsPoints.infoWindow.open(googleMapsPoints.map, marker);
    });

    return marker;
};

//Show markers on map
googleMapsPoints.showMarkersOnMap = function() {

	for(var i in googleMapsPoints.markers){
		googleMapsPoints.markers[i]['marker'].setMap(googleMapsPoints.map);
	}
	//if (googleMapsPoints.isLinesLoading != null && googleMapsPoints.isLinesLoading == true) {
		for(var i = 0; i <= googleMapsPoints.layersCount; i++){
			var checkboxName = googleMapsPoints.checkboxLayerPrefix + i;
			if (document.interfacePoints[checkboxName].checked) {
				googleMapsPoints.visibileLayer(document.interfacePoints[checkboxName].value,true);
			} else {
				googleMapsPoints.visibileLayer(document.interfacePoints[checkboxName].value,false);
			}
		}
	//}
}

//Change layer visibility
googleMapsPoints.changeLayerVisibility = function(field, index){
	var form = field.form;
	if(field.type != 'checkbox'){
		field = form[googleMapsPoints.checkboxLayerPrefix + index];
		field.checked = !field.checked;
	}
	if(index > 0){
		checkboxName = googleMapsPoints.checkboxLayerPrefix + 0;
		form[checkboxName].checked = false;
		googleMapsPoints.visibileLayer(field.value, field.checked);
	}
	else {
		var checkboxName;
		for(var i = 0; i <= googleMapsPoints.layersCount; i++){
			checkboxName = googleMapsPoints.checkboxLayerPrefix + i;
			form[checkboxName].checked = field.checked;
			googleMapsPoints.visibileLayer(form[checkboxName].value, field.checked);
		}
	}
};

//Check layer visibility
googleMapsPoints.visibileLayer = function(layerId, isVisible){
	for(var i in googleMapsPoints.markers){
		if(googleMapsPoints.markers[i]['layerId'] == layerId){
			with(googleMapsPoints.markers[i]['marker']){
				isVisible ? setMap(googleMapsPoints.map) : setMap(null);
			}
		}
	}
};

//Open infoWindow on dropdown select
googleMapsPoints.openInfoWindow = function(pointId){
	for(var i in googleMapsPoints.markers){
		var layerId = googleMapsPoints.markers[i]['layerId'];
		if(i == pointId){
			google.maps.event.trigger(googleMapsPoints.markers[i]['marker'], 'click');

			googleMapsPoints.visibileLayer(layerId, true);
			for(var i = 0; i <= googleMapsPoints.layersCount; i++){
				var checkboxName = googleMapsPoints.checkboxLayerPrefix + i;
				if (document.interfacePoints[checkboxName].value == layerId) {
					document.interfacePoints[checkboxName].checked = true;
				}
			}
		}
	}
};