googleMapsConfig = {};

googleMapsConfig.position = {
	  X: 52.138019
	, Y: 21.211996
};
googleMapsConfig.icon = {
	  width:  35
	, height:  55
};

googleMapsConfig.zoom = 13;

googleMapsConfig.loadAPI = function(callback){
	var script = document.createElement('script');
		script.setAttribute('src', 'http://maps.google.com/maps/api/js?sensor=true&libraries=geometry' + '&callback=' + callback);
		script.setAttribute('type', 'text/javascript');
	document.documentElement.firstChild.appendChild(script);
};