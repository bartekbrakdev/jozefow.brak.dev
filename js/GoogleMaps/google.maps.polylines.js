googleMapsPolylines = {};

googleMapsPolylines = {};
googleMapsPolylines.map 				= null;
googleMapsPolylines.linesXmlFile 	    = '';
googleMapsPolylines.iconsPath 		    = '';
googleMapsPolylines.checkboxLayerPrefix = 'layer';
googleMapsPolylines.polyLines		    = [];
googleMapsPolylines.centerX 			= googleMapsConfig.position.X;
googleMapsPolylines.centerY 			= googleMapsConfig.position.Y;
googleMapsPolylines.zoom				= googleMapsConfig.zoom;

//Initialization
googleMapsPolylines.init = function(){

	var mapOptions = {
			zoom: googleMapsPolylines.zoom,
			center: new google.maps.LatLng(googleMapsPolylines.centerX, googleMapsPolylines.centerY),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	googleMapsPolylines.map = new google.maps.Map(document.getElementById("mapBox"), mapOptions);
    googleMapsPolylines.loadLinesFromXML();

};

//Load polylines/polygons from XML file
googleMapsPolylines.loadLinesFromXML = function() {

	var attributes 	= ['id','name','desc','link','layerId','isPolygon','lineColor','lineWeight', 'fillColor','fillOpacity','zoom','x','y','lineCode','institutionId','languageId'];

	$.ajax({
	      type: 	"GET",
	      url: 		googleMapsPolylines.linesXmlFile,
	      dataType: "xml",
	      success: function(data) {
			 xml = $(data);
			 xml.find("line").each(function(){
	       		 var v = {};
	           	 for(var j = 0; j < attributes.length; j++){
	           		v[attributes[j]] = $(this).attr(attributes[j]).replace(/&quot;/g,'"');
	           	 }
	   			 var id = $(this).attr('id');
	   			 var layerId = $(this).attr('layerId');

	   		        googleMapsPolylines.polyLines.push({
				  			'id'        : id,
				  			'layerId'   : layerId,
				  			'item'      : v,
				  			'polyShape' : googleMapsPolylines.drawPolyShape(v)
			  		});
			 });
	      }
	});

};

//Draw polyline/polygon
googleMapsPolylines.drawPolyShape = function(lineItem){
  var polyPoints  = googleMapsPolylines.decodeLine(lineItem.lineCode);

  if(parseInt(lineItem.isPolygon) == 1) {
	  var polyShape = new google.maps.Polygon({
		 path: 			polyPoints,
		 strokeColor: 	lineItem.lineColor,
		 strokeWeight: 	lineItem.lineWeight,
		 strokeOpacity:	1,
		 fillColor:		lineItem.fillColor,
		 fillOpacity:	lineItem.fillOpacity,
		 map:           googleMapsPolylines.map
	  });
  } else {
	  var polyShape = new google.maps.Polyline({
		 path: 		 	polyPoints,
		 strokeColor:	lineItem.lineColor,
		 strokeOpacity: 1,
		 strokeWeight:  lineItem.lineWeight,
		 map:           googleMapsPolylines.map
	  });
  }
 return polyShape;
};

//Decode an encoded polyline into a list of lat/lng.
googleMapsPolylines.decodeLine = function(encoded) {
	var array = google.maps.geometry.encoding.decodePath(encoded);
 return array;
};

//Change layer visibility
googleMapsPolylines.changeLayerVisibility = function(field, index){
	var form = field.form;
	if(field.type != 'checkbox'){
		field = form[googleMapsPoints.checkboxLayerPrefix + index];
		field.checked = !field.checked;
	}
	if(index > 0){
		checkboxName = googleMapsPoints.checkboxLayerPrefix + 0;
		form[checkboxName].checked = false;
		googleMapsPolylines.visibileLayer(field.value, field.checked);
	}
	else {
		var checkboxName;
		for(var i = 0; i <= googleMapsPolylines.polyLines.length; i++){
			checkboxName = googleMapsPoints.checkboxLayerPrefix + i;
			form[checkboxName].checked = field.checked;
			googleMapsPolylines.visibileLayer(form[checkboxName].value, field.checked);
		}
	}
};

//Check layer visibility
googleMapsPolylines.visibileLayer = function(layerId, isVisible){
	for(var i in googleMapsPolylines.polyLines){
		if(googleMapsPolylines.polyLines[i]['layerId'] == layerId){
			with(googleMapsPolylines.polyLines[i]['polyShape']){
				isVisible ? setMap(googleMapsPolylines.map) : setMap(null);
			}
		}
	}
};