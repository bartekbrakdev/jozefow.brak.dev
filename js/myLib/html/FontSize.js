/**
 * @fileoverview Pozwala powieksza� i zmniejsza� rozmiar fontow w ramach
 * okre�lonego elementu html (this.elementId)
 * <div id="mainContent">content</div>
 *
 * @used IE, NN, FireFox, Opera
 *
 * @example
 *		var fontSize = new FontSize();
 *		fontSize.addEvents();
 *
 * @version 1.2 2005/01/06
 */

Import('myLib/events/CreatorEvents.js');

/**
 * @constructor
 * @param	Mixed	Element strony lub id elementu.
 */

function FontSize(element) {
	/**
	 * Id elementu w kt�rym b�dzie zmieniany rozmiar czcionki.
	 * @type	String
	 * @public
	 */
	this.id = 'subpage_content';

	/**
	 * Domy�lny rozmiar czcionki.
	 * @type	Number
	 * @public
	 */
	this.size = 12;

	/**
	 * Stan klawisz shift.
	 * @type	Number
	 * @private
	 */
	this.shift = 0;

	/**
	 * Zawiera element w kt�rym b�dzie zmieniany rozmiar czcionki.
	 * @type	Object
	 * @private
	 */
	this.element = null;

	if(typeof(element) == 'object'){
		this.element = element;
	}
	else if(typeof(element) == 'string'){
		this.element = document.getElementById(element);
	}
	else{
		this.element = document.getElementById(this.id);
	}
};

FontSize.size = 12;

/**
 * Przechowuje aktualny obiekt klasy FontSize.
 * @type	Object
 * @private
 */
FontSize.self = null;

/**
 * Dodaje zdarzenia umo�liwiaj�ce powi�kszanie tekstu (klaiwsz +) oraz
 * zmniejszanie tekst (klawisz -).
 * @public
 * @return	void
 */
FontSize.prototype.addEvents = function(){
	FontSize.self = this;
	new CreatorEvents(window).add('keydown', function(e){ FontSize.self.resize(e); });
	new CreatorEvents(window).add('keyup', function(e){ FontSize.self.clearShift(e); });
};

/**
 * Resetuje zmienn� this.shift je�eli zosta� wci�ni�ty klawisz szift.
 * @public
 * @return	void
 */
FontSize.prototype.clearShift = function(e){
	if(window.event)
		var key = window.event.keyCode;
	else
		var key = e.which;

	if(key == 16){
		this.shift = 0;
	}
};

/**
 * @public
 * @param	Object	Zdarzenie wywo�uj�ce funkcj�.
 * @return	void
 */
FontSize.prototype.resize = function(e) { // for keys
	var key, keyMinus, keyPlus;

	if(window.event){
		key = window.event.keyCode;
		keyMinus = 189 // inny kod klawisza + dla IE
		keyPlus = 187; // inny kod klawisza + dla IE
	}
	else{
		key = e.which;
		keyMinus = 109 // w gecko ten sam kod dla klawisza
		keyPlus = 61;
	}

	//alert(key);

	if(key == 16)
		this.shift = key;
	if(key == 109 || (this.shift == 16 && key == keyMinus))
		this.resizeFont(false);
	if(key == 107 || (this.shift == 16 && key == keyPlus))
		this.resizeFont(true);
};

/**
 * Zmienia rozmiar czcionki w zale�no�ci od przekazanej zmiennej (true +, false -).
 * @public
 * @param	Boolean	Okre�la czy powi�kszy�, czy zmniejszy� czcionk�.
 * @return	void
 */
FontSize.prototype.resizeFont = function(quantity){ // for mouse
	var tmp = parseInt(FontSize.size);

	if(!quantity && tmp > 12)
		tmp -= 1;
	if(quantity && tmp < 16)
		tmp += 1;
	FontSize.size = tmp + 'px';

	this.changeSize(this.element);
};

/**
 * Zmienia rozmiar czcionki dla podanego elementu oraz
 * dla wszystkich podrz�dnych element�w.
 * @private
 * @param	Object	Element strony w kt�rym ma by� zmieniony rozmiar czcionki.
 * @return	void
 */
FontSize.prototype.changeSize = function(element){
	if(element == null){
		return;
	}
	if(element.nodeName != '#text' && element.nodeName != 'DIV'){
		element.style.fontSize = FontSize.size;
	}

	for(var i = 0; i < element.childNodes.length; i++){
		this.changeSize(element.childNodes[i]);
	}
};