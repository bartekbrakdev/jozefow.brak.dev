/**
 * example
 *		DisplayFlash.init({src: 'pliki/myFlash.swf', width: 400, height: 80});
 *		or
 *		var df = new DisplayFlash({src: 'pliki/myFlash.swf', width: 400, height: 80});
 *		df.insertCode();
 *		
 * version 1.0 2005/07/18
 */
 
Import('myLib/util/Random.js');
Import('myLib/native/Array.js');

DisplayFlash = function(params){

	this.classid = 'clsid:d27cdb6e-ae6d-11cf-96b8-444553540000';
	
	this.params = {
		id: Random.string(10),
		src: '',
		quality: 'high',
		wmode: 'transparent',
		bgcolor: '#ffffff'
	};
	
	for(var i in params){
		this.params[i] = params[i];
	}
};

DisplayFlash.init = function(params){
	var df = new DisplayFlash(params);
	df.insertCode();
	return df; // df.getId();
};

DisplayFlash.prototype.insertCode = function(){
	document.write(this.getCode());
};

DisplayFlash.prototype.getCode = function(){
	return '<object ' + this.getParamsObject() + ' classid="' + this.classid + '" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0">' +
	'<param name="movie" value="' + this.params['src'] + '" />' +
	'<param name="quality" value="' + this.params['quality'] + '" />' +
	'<param name="wmode" value="' + this.params['wmode'] + '" />' +
	'<param name="bgcolor" value="' + this.params['bgcolor'] + '" />' +
	'<embed ' + this.getParamsEmbed() + ' type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>' +
	'</object>';
};

DisplayFlash.prototype.getParamsObject = function(){
	var tmp = {};
	var keys = ['id', 'width', 'height'];
	for(var i = 0; i < keys.length; i++){
		if(typeof(this.params[keys[i]]) != 'undefined'){
			tmp[keys[i]] = this.params[keys[i]];
		}	
	}
	return Array.getString(tmp);
};

DisplayFlash.prototype.getParamsEmbed = function(){
	var tmp = {};
	for(var i in this.params){
		if(i == 'id')
			tmp['name'] = this.params[i];
		else
			tmp[i] = this.params[i];	
	}
	return Array.getString(tmp);
};

DisplayFlash.prototype.setParam = function(key, value){
	this.params[key] = value;
};

DisplayFlash.prototype.getParam = function(key){
	return this.params[key];
};

DisplayFlash.prototype.getId = function(){
	return this.getParam('id');
};