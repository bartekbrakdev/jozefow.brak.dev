/**
 * @fileoverview Sprawdza pola formularza.
 *
 * @used IE, FF, Opera
 *
 * @example
 *		var x = new CheckForm(this, [alerts type]);
 *		x.emptyField('field1', 'field2');
 *
 * @version 1.5 2006/02/09
 */

Import('myLib/native/String.js');
Import('myLib/native/Array.js');
Import('myLib/native/Number.js');
Import('myLib/form/Form.js');

/**
 * @constructor
 * @public
 * @param	Object	Formularz lub pole formularza.
 * @param	Boolean	<td><span id="labels.nazwa">Nazwa</span>:</td>
 * @return	void
 */
function CheckForm(obj, isLabel){ //System.subClass();
	this.obj = obj;
	var isLabel = System.getValue(isLabel, false);
	this.type = 0;
	if(isLabel){
		this.type = 1;
	}
};

CheckForm.ALL_FIELDS = '*';

/**
 * @type	Array
 * @private
 */
CheckForm.alerts = {
     0: [
     	'Proszę wypełnić wskazane pole.',
     	'Proszę wypełnić pole: {name}.'
     ],
     1: 'Proszę wypełnić wszystkie pola.',
     2: [
     	'Proszę wypełnić przynajmniej jedno pole.',
     	'Proszę wypełnić przynajmniej jedno z podanych pól: {name}.'
     ],
     3: 'Niepoprawny format daty. Podaj: RRRR{token}MM{token}DD.',
     4: 'Data musi być bieżąca lub przyszła.',
     5: 'Niepoprawny format godziny.',
     6: [
     	'Proszę we wskazane pole wpisać liczbę.',
     	'Proszę w pole {name} wpisać liczbę.'
     ],
     7: 'Podaj liczbę z przedziału od {x} do {y}.',
     8: 'Przekroczyłeś maksymalną liczbę znaków równą: {max}.',
     9: 'Liczba znaków musi być równa: {length}.',
     10: 'Adres e-mailowy nie jest poprawny.',
     11: 'Nazwa pliku może mieć maksymalnie {count} znaków.',
     12: 'Niepoprawny format pliku.\nDozwolone formaty: {format}.',
     13: 'Kod pocztowy nie jest poprawny.',
     14: 'Już istnieje.',
     15: 'Hasła muszą być identyczne',
     16: 'Zaznaczona data musi być póniejsza.',
     17: 'Zaznaczona data musi być wcześniejsza.',
     18: 'Zaznaczona data musi być równa.'
};

/**
 * @type	Array
 * @public
 * @final
 */
CheckForm.type = {
	IMAGE: 		1,
	DOCUMENT:	2,
	MUSIC:		4,
	MOVIE:		8,
	OTHER:		16
};

/**
 * @type	Object
 * @private
 */
CheckForm.formatsFile = {
	1:	[ 'jpg', 'gif', 'png' ],
	2:	[ 'doc', 'pdf', 'xls', 'htm', 'html' ],
	4:	[ 'mp3', 'wav' ],
	8:	[ 'swf', 'avi', 'wma' ],
	16:	[ 'zip', 'rar', 'exe' ]
};

/**
 * @private
 * @return	Boolean
 */
CheckForm.prototype.checkObject = function(){
	return (this.obj.value == '' || this.obj.disabled);
};

/**
 * @private
 * @param	Mixed
 * @return	Object
 */
CheckForm.prototype.getElement = function(element){
	var r = null;
	if(typeof(element) == 'undefined'){
		r = this.obj;
	}
	else{
		if(element instanceof Array)
			r = element[0];
		else
			r = element;
	}
	return r;
};

/**
 * @private
 * @param	Number
 * @param	String
 * @return	void
 */
CheckForm.prototype.setAlert = function(key, value){
	CheckForm.alerts[key] = value;
};

/**
 * @private
 * @param	String
 * @param	Mixed
 * @param	Object
 * @return	String
 */
CheckForm.prototype.focusAlert = function(text, element, strings){
	this.errorAlert(text, element, strings);
	var field = this.getElement(element);
	setTimeout(function(){field.focus();}, 200);
	return false;
};

/**
 * @private
 * @param	String
 * @param	Mixed
 * @param	Object
 * @return	String
 */
CheckForm.prototype.selectAlert = function(text, element, strings){
	this.errorAlert(text, element, strings);
	var field = this.getElement(element);
	setTimeout(function(){field.focus();}, 200);
	field.select();
	return false;
};

/**
 * @private
 * @param	String
 * @param	Mixed
 * @param	Object
 * @return	Boolean
 */
CheckForm.prototype.errorAlert = function(text, element, strings){
	if(typeof(text) == 'number'){
		var a = CheckForm.alerts;
		if(typeof(a[text]) == 'object')
			text = a[text][this.type];
		else
			text = a[text];
	}
	AlertBox.alert(this.createAlert(text, element, strings));
	return false;
};

/**
 * @private
 * @param	String
 * @param	Mixed
 * @param	Object
 * @return	String
 */
CheckForm.prototype.createAlert = function(text, element, strings){
	var value = '';
	var t = new Array();

	if(typeof(element) != 'object'){
		t[0] = this.obj;
	}
	else{
		if(element instanceof Array)
			t = element;
		else
			t[0] = element;
	}

	for(var i = 0; i < t.length; i++){
		var name = t[i].name;
		var z = document.getElementById('labels.' + name);
		if(z != null){
			if(value != '')
				value += ', ';
			value += z.innerHTML;
		}
	}

	if(typeof(strings) == 'object'){
		for(var i in strings){
			text = text.replaceAll('{' + i + '}', strings[i]);
		}
	}

	return text.replaceAll('{name}', value);
};

//--------------------------------------------------------------------------

/**
 * @public
 * @return	Boolean
 */
CheckForm.prototype.emptyField = function(inputs){
	var fields = typeof(inputs) == 'string' ? this.emptyField.arguments : inputs;
	var isAll = fields[0] == CheckForm.ALL_FIELDS;

	for(var i = 0; i < fields.length; i++){
		for(var j = 0; j < this.obj.elements.length; j++){
			if(
				   this.obj.elements[j].disabled
				|| this.obj.elements[j].type == 'button'
				|| this.obj.elements[j].type == 'submit'
			){
				continue;
			}
			if(isAll || fields[i] == this.obj.elements[j].name){
				if(this.obj.elements[j].type == 'radio'){
					var name = this.obj.elements[j].name;
					var isFill = false;
					for(var z = 0; z < this.obj[name].length; z++){
						if(this.obj[name][z].checked){
							isFill = true;
							break;
						}
					}
					if(!isFill){
						return this.focusAlert(isAll ? 1 : 0, this.obj.elements[j]);
					}
				}
				else if(this.obj.elements[j].type == 'checkbox'){
					if(!this.obj.elements[j].checked){
						return this.focusAlert(isAll ? 1 : 0, this.obj.elements[j]);
					}
				}
				else if(this.obj.elements[j].type == 'select-multiple'){
					var isFill = false;
					for(var z = 0; z < this.obj.elements[j].options.length; z++){
						if(this.obj.elements[j].options[z].selected){
							isFill = true;
							break;
						}
					}
					if(!isFill){
						return this.focusAlert(isAll ? 1 : 0, this.obj.elements[j]);
					}
				}
				else{
					if(this.obj.elements[j].value == ''){
						return this.focusAlert(isAll ? 1 : 0, this.obj.elements[j]);
					}
				}
			}
		}
	}

	return true;
};

/**
 * @public
 * @return	Boolean
 */
CheckForm.prototype.fillOneField = function(inputs){
	var fields = typeof(inputs) == 'string' ? this.fillOneField.arguments : inputs;
	var isAll = fields[0] == CheckForm.ALL_FIELDS;

	for(var i = 0; i < fields.length; i++){
		for(var j = 0; j < this.obj.elements.length; j++){
			if(
				   this.obj.elements[j].disabled
				|| this.obj.elements[j].type == 'button'
				|| this.obj.elements[j].type == 'submit'
			){
				continue;
			}
			if(isAll || fields[i] == this.obj.elements[j].name){
				if(this.obj.elements[j].type == 'radio'){
					var name = this.obj.elements[j].name;
					for(var z = 0; z < this.obj[name].length; z++){
						if(this.obj[name][z].checked){
							return true;
						}
					}
				}
				else if(this.obj.elements[j].type == 'checkbox'){
					if(this.obj.elements[j].checked){
						return true;
					}
				}
				else if(this.obj.elements[j].type == 'select-multiple'){
					for(var z = 0; z < this.obj.elements[j].options.length; z++){
						if(this.obj.elements[j].options[z].selected){
							return true;
						}
					}
				}
				else{
					if(this.obj.elements[j].value != ''){
						return true;
					}
				}
			}
		}
	}

	return this.focusAlert(2, this.obj[fields[0]]);
};

/**
 * @public
 * @param	String
 * @return	Boolean
 */
CheckForm.prototype.date = function(token){
	if(this.checkObject()){
		return true;
	}
	var token = System.getValue(token, '-');
	var pattern = new RegExp();
	var year = 0;
	var month = 0;
	var day = 0;
	var val = this.obj.value;

	pattern.compile('^(\\d{4})' + token + '([0-9]{2})' + token + '(\\d{2})$', 'g');

	if(pattern.test(val)){
		year = parseInt(val.replace(pattern, '$1'));
		month = val.replace(pattern, '$2');
		month = Number.parseInt(month);
		day = val.replace(pattern, '$3');
		day = Number.parseInt(day);
	}

	if(year < 1000 || year > 3000 || month < 1 || month > 12 || day < 1 || day > 31){
		this.obj.value = '';
		return this.selectAlert(3, this.obj, { 'token': token });
	}

	return true;
};

/**
 * @public
 * @param	String
 * @return	Boolean
 */
CheckForm.prototype.currentDate = function(token){
	if(this.checkObject()){
		return true;
	}
	var token = System.getValue(token, '-');
	if(!this.date(token)){
		return false;
	}

	var today = new Date();
	var year = today.getYear();
	var month = today.getMonth() + 1;
	var day = today.getDate();
	var tab = this.obj.value.split(token);

	if(tab[1].substr(0, 1) == '0')
		tab[1] = Number.parseInt(tab[1].substr(1, 1))
	else
		tab[1] = Number.parseInt(tab[1]);

	if(tab[2].substr(0, 1) == '0')
		tab[2] = Number.parseInt(tab[2].substr(1, 1))
	else
		tab[2] = Number.parseInt(tab[2]);

	if((tab[0] < year) || (tab[0] == year && tab[1] < month) ||
	(tab[0] == year && tab[1] == month && tab[2] < day)){
		return this.selectAlert(4);
	}

	return true;
};

/**
 * @public
 * @param	Object	Pole formularza.
 * @param	String	Token.
 * @param	Boolean
 */
CheckForm.prototype.majorDate = function(field, token){
    return this.checkDate(field, token, 1, 16);
};

CheckForm.prototype.minorDate = function(field, token){
    return this.checkDate(field, token, -1, 17);
};

CheckForm.prototype.equalDate = function(field, token){
    return this.checkDate(field, token, 0, 18);
};

CheckForm.prototype.checkDate = function(field, token, value, alert){
    if(this.checkObject() || field.value == ''){
        return true;
    }
    if(Date.compareDates(this.obj.value, field.value, token) != value){
    	this.obj.value = '';
        return this.selectAlert(alert, field);
    }
    return true;
};

/**
 * @public
 * @param	String
 * @return	Boolean
 */
CheckForm.prototype.hour = function(token){
	if(this.checkObject()){
		return true;
	}
	var token = System.getValue(token, ':');
	var tab = this.obj.value.split(token);
	// || tab[0].length != 2
	if((isNaN(tab[0]) || tab[0] < 0 || tab[0] > 23 || !tab[0]) ||
	(isNaN(tab[1]) || tab[1].length != 2 || tab[1] < 0 || tab[1] > 59 || !tab[1])){
		this.obj.value = '';
		return this.selectAlert(5);
	}
	return true;
};

/**
 * @public
 * @return	Boolean
 */
CheckForm.prototype.number = function(){
	if(this.checkObject()){
		return true;
	}
	if(isNaN(this.obj.value)){
		return this.selectAlert(6);
	}
	return true;
};

/**
 * @public
 * @param	Number	Od
 * @param	Number	Do
 * @return	Boolean
 */
CheckForm.prototype.scopeNumber = function(x, y){
	if(this.checkObject()){
		return true;
	}
	if(!this.number()){
		return false;
	}
	var val = Number.parseInt(this.obj.value);
	if(val < x || val > y){
		return this.selectAlert(7, this.obj, { 'x': x, 'y': y });
	}
	return true;
};

/**
 * @public
 * @param	Number	Maksymalna rozmiar pola. Domyslnie: 65535.
 * @return	Boolean
 */
CheckForm.prototype.maxLength = function(max){
	if(this.checkObject()){
		return true;
	}
	var max = System.getValue(max, 65535);
	if(this.obj.value.length > max) {
		this.obj.value = this.obj.value.substring(0, max);
		return this.focusAlert(8, this.obj, { 'max': max });
	}
	return true;
};

/**
 * @public
 * @event	onfocus
 */
CheckForm.maxLength = function(field, max){
	var f = function(e){
		new CheckForm(field).maxLength(max);
	}
	field.onkeyup = f;
	field.onblur = f;
};

/**
 * @public
 * @param	Number
 * @return	Boolean
 */
CheckForm.prototype.length = function(length){
	if(this.checkObject()){
		return true;
	}
	if(this.obj.value.length != length) {
		return this.focusAlert(9, this.obj, {'length': length});
	}
	return true;
};

/**
 * @public
 * @return	Boolean
 */
CheckForm.prototype.zipCode = function(){
	if(this.checkObject()){
		return true;
	}
	var pattern = /^[0-9]{2}(-)[0-9]{3}$/;
	if(!pattern.test(this.obj.value)){
		return this.selectAlert(13);
	}
	return true;
};

CheckForm.checkMail = function(value){
	var pattern = /^[0-9a-z\._-]+(@|\(at\))+[0-9a-z\._-]+\.[a-z]{2,3}$/i;
	return !(!pattern.test(value) || value.lastIndexOf('@') != value.indexOf('@'));
};

CheckForm.prototype.mail = function(){
	if(this.checkObject()){
		return true;
	}
	if(!CheckForm.checkMail(this.obj.value)) {
		this.obj.value = '';
		return this.selectAlert(10);
	}
	return true;
};

CheckForm.prototype.manyMail = function(){
	if(this.checkObject()){
		return true;
	}
	var val = this.obj.value;
	var mails = this.obj.value.split(',');
	for(var i = 0; i < mails.length; i++){
		if(!CheckForm.checkMail(mails[i].trim())){
			return this.selectAlert(10);
		}
	}
	return true;
};

/**.
 * @public
 * @param	Object	Pole formularza.
 * @return	Boolean
 */
CheckForm.prototype.password = function(field){
    if(this.checkObject() || field.value == ''){
        return true;
    }
    if(this.obj.value != field.value) {
        return this.selectAlert(15);
    }
    return true;
};

/**
 * @public
 * @param	Array
 * @return	Boolean
 */
CheckForm.prototype.exist = function(values){
	if(this.checkObject()){
		return true;
	}
	if(Form.existValue(this.obj, values)){
		return this.selectAlert(14);
	}
	return true;
};

CheckForm.prototype.existConfirm = function(values, href, text){
	if(this.checkObject()){
		return true;
	}
	for(var i in values){
		if(values[i].toLowerCase() == this.obj.value.toLowerCase()){
			var text = text || 'Podany element już istnieje.';
			text += '\nCzy chcesz to sprawdzić?';
			AlertBox.confirm(text, {onComplete: function(e) {
				if(e) {
					href = href.replaceByArray({id: i});
					window.open(href);
				}
			  }
			});
			return false;
		}
	}
	return true;
};

/**
 * @deprecated
 * @public
 * @return	Boolean
 */
CheckForm.prototype.nameFile = function(){
	if(this.checkObject()){
		return true;
	}
	var count = 128;
	var name = this.obj.value.cutLast(/[\/\\]/);
	if(name.length > count){
		return this.focusAlert(11, this.obj, {'count': count});
	}
	return true;
};

/**
 * @public
 * @param	Mixed
 * @return	Boolean
 */
CheckForm.prototype.formatsFile = function(formats){
	if(this.checkObject()){
		return true;
	}

	if(typeof(formats) != 'object'){
		formats = CheckForm.getFormatsFile(formats);
	}
	var suffix = this.obj.value.substring(this.obj.value.length - 3);
	var string = '';

	for(var i = 0; i < formats.length; i++){
		if(formats[i].toLowerCase() == suffix.toLowerCase()){
			return true;
		}
		if(string != ''){
			string += ', ';
		}
		string += formats[i];
	}

	return this.focusAlert(12, this.obj, {'format': string});
};

/**
 * @private
 * @param	Number
 * @return	Array
 */
CheckForm.getFormatsFile = function(type){
	var type = System.getValue(type, Array.sum(CheckForm.type));
	var pattern = CheckForm.formatsFile;
	var temp = [];

	for(var i in pattern){
		if(i & type){
			for(var j in pattern[i]){
				temp[temp.length] = pattern[i][j];
			}
		}
	}

	return temp;
};
