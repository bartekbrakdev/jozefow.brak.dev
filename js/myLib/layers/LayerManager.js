/**
 * @fileoverview Pozwala na wyswietlenie wypozycjonowanej warstwy (zastepuje pop-upa)
 *
 * @used IE, FF, Opera
 *
 */

//Import('myLib/util/Browser.js');
Import('myLib/events/CreatorEvents.js');
Import('myLib/native/Array.js');
Import('myLib/html/DisplayFlash.js');
Import('myLib/layers/DisplayLayer.js');
Import('myLib/html/DisplayImageMap.js');
Import('myLib/html/Tag.js');

/**
 * @constructor
 * @param	String	Id elementu strony.
 * @param	Boolean	Optional, default: true. Okresla czy warstwa ma sie wyswietlac.
 */

DisplayLayer = {};

DisplayLayer.close = function(id) {
	LayerManager.close(id);
};

function LayerManager(id, isDisplay){
	this.id = System.getValue(id, LayerManager.DEFAULT_ID);
	this.isDisplay = System.getValue(isDisplay, true);

	LayerManager.objects[this.id] = this;

	this.style = {
		visibility: 'visible',
		left: 		-1,
		top: 		-1,
		width:		100,
		height:		100,
		'z-index': 	1000
	};

	this.offsetX = 0;
	this.offsetY = 0;
};

LayerManager.objects = {};

LayerManager.DEFAULT_ID = 'warstwa1';

LayerManager.close = function(id){
	id = System.getValue(id, LayerManager.DEFAULT_ID);
	var layer = document.getElementById(id);
	layer.style.visibility = 'hidden';
};

//---

LayerManager.prototype.setOffset = function(offsetX, offsetY){
	this.offsetX = offsetX;
	this.offsetY = offsetY;
};

LayerManager.prototype.setStyle = function(style){
	for(var i in style){
		this.style[i] = style[i];
	}
};

LayerManager.prototype.loadParams = function(params){
	params['width'] = this.style['width'];
	params['height'] = this.style['height'];
};

//---

LayerManager.prototype.insertFlash = function(params, areas){
	this.loadParams(params);
	var df = new DisplayFlash(params);
	this.insertCode(df.getCode());
};

LayerManager.prototype.insertImageMap = function(params, areas){
	this.loadParams(params);
	var dim = new DisplayImageMap(params);
	dim.setAreas(areas);
	this.insertCode(dim.getCode());
};

LayerManager.prototype.insertCode = function(content){
	if(this.isDisplay){
		var ce = new CreatorEvents(window);
		ce.append('resize', 'LayerManager.objects[\'' + this.id + '\'].initPosition()');

		var style = {};
		for(var i in this.style){
			style[i] = this.style[i];
		}
		style['position'] = 'absolute';
		this.initPosition(style);

		var styleString = Array.getString(style, ' {key}: {value};');

		var t = new Tag(Tag.DIV);
		t.setText(content);
		t.insertCode({'id': this.id, 'style': styleString});
	}
};

//---

LayerManager.prototype.initPosition = function(style){
	var layer = document.getElementById(this.id);
	if(layer != null){
		style = layer.style;
	}
	if(this.style['left'] == -1){
		style['left'] = this.getPositionX(this.style['width']) + 'px';
	}
	if(this.style['top'] == -1){
		style['top'] = this.getPositionY(this.style['height']) + 'px';
	}
};

LayerManager.prototype.getPositionX = function(widthLayer){
	var clientWidth = (document.documentElement || document.body).clientWidth;
	return this.getPosition(clientWidth, widthLayer, this.offsetX);
};

LayerManager.prototype.getPositionY = function(heightLayer){
	var clientHeight = (document.documentElement || document.body).clientHeight;
	return this.getPosition(clientHeight, heightLayer, this.offsetY);
};

LayerManager.prototype.getPosition = function(windowSize, layerSize, offset){
	var position = offset;
	if(windowSize > layerSize){
		position = (windowSize / 2) - (layerSize / 2) + offset;
	}
	return Math.floor(position);
};