/**
 * @fileoverview Importuje podany plik.
 *
 * @Used IE, FF, Opera
 *
 * @example
 *		Import('myLib/data/Vector.js');
 *
 */

/**
 * @constructor
 * @param	String
 */
function Import(file){
	file = Import.getScriptPath(file);
	Import.script(Import.getPath() + file);
};

Import.getScriptPath = function(file){
	if(file.indexOf('/') >= 0)
		return file;
	else
		return file.split('.').join('/') + '.js';
};


Import.getPath = function(){
	return Config.getPathToJS();
};

Import.script = function(file){
	if(!Import.isImport(file)){
		document.write('<script src="' + file + '"></script>');
	}
};

Import.isImport = function(file){
	var scripts = document.getElementsByTagName('script');
	for(var i = 0; i < scripts.length; i++){
		if(scripts[i].getAttribute('src') == file){
			return true;
		}
	}
	return false;
};

Import.namespace = function(name){
    if(typeof(name) == 'undefined'){
        alert('Należy podać nazwę pakietu.');
        return false;
    }
    name = name.replace('.', '/');
    if(name.substr(name.length - 1, 1) != '/'){
        name += '/';
    }
    var result = false;
    var scripts = document.getElementsByTagName('script');
    for(var i = 0; i < scripts.length; i++){
        if(scripts[i].src != ''){
            var tmp = scripts[i].src.replace(Import.getPath(), '');
            if(tmp.substr(0, name.length) == name){
                tmp = tmp.replace('.js', '');
                var className = tmp.replace(name, '');
                var classFullName = tmp.replace('/', '.');
                eval(className + ' = ' + classFullName);
                result = true;
            }
        }
    }
    return result;
};

Import.style = function(file){
	var link = document.createElement('link');
	link.setAttribute('href', Import.getPath() + file);
	link.setAttribute('rel', 'stylesheet');
	link.setAttribute('type', 'text/css');

	var head = document.getElementsByTagName('head')[0];
	head.appendChild(link);
};

Import.getFullPath = function(file){
	var path = Import.getPath();
	if(typeof(file) != 'undefined'){
		path += file;
	}
	return path;
};

Import.scriptList = function(){
	var content = '<ul>';
	for(var i = 0; i < Import.scripts.length; i++){
		content += '<li>' + Import.scripts[i] + '</li>';
	}
	content += '</ul>';
	document.write(content);
};

Import.load = function(){
    var obj, tmp;
    for(var j = 0; j < Import.scripts.length; j++){
        var script = Import.scripts[j];
        var m = script.substring(0, script.lastIndexOf('.'));
        var t = m.split('/');
        for(var i = 0; i < t.length; i++){
            obj = t[i];
            if(i == 0){
                if(!window[obj]){
                    window[obj] = new Object();
                }
                tmp = window[obj];
            }
            else if(i < t.length-1){
                if(!tmp[obj]){
                    tmp[obj] = new Object;
                }
                tmp = tmp[obj];
            }
            else{
                tmp[obj] = window[obj];
            }
        }
    }
};

Import.clone = function(src, desc){
	if(typeof(desc) != 'object'){
		var desc = {};
	}
	for(var i in src){
		if(typeof(src[i]) == 'object'){
			desc[i] = {};
			Import.clone(src[i], desc[i]);
		}
		else{
			desc[i] = src[i];
		}
	}
	return desc;
};

Import('myLib/System.js');
