Service = {};

Service.$E = function(id){
	return document.getElementById(id);
};


Service.onLoad = function(newFunc){
	var evname = 'onload';
	if(typeof(window[evname]) == 'function')
		var oldFunc = window[evname];
	else
		var oldFunc = function(){};
	window[evname] = function(){
		oldFunc();
		newFunc();
	};
};

Service.createTargetBlank = function(){
	var items = document.getElementsByTagName('a');
	for(var i = 0; i < items.length; i++){
		if(items[i].target == '_blank'){
			items[i].target = '_self';
			items[i].href = "javascript:void(window.open('" + items[i].href + "'))";
		}
	}
};

Service.showMail = function(mail, label){
	mail = Crypt.decode(mail);
	if(typeof(label) == 'undefined')
		label = mail;
	else
		label = Crypt.decode(label);
	document.write('<a href="mailto:' + mail + '">' + label + '</a>');
};

Service.sendSearchForm = function(field){
	if(field.value == ''){
		AlertBox.alert('Podaj szukane wyrażenie.');
		field.focus();
	}
	else{
		field.form.submit();
	}
	return false;
};

Service.addTooltip = function(element, text){
	element.alt = text;
};


Service.getOpener = function(){
	if(document.opener)
		return document.opener;
	else
		return opener;
};

Service.getOpenerParent = function(){
	return Service.getOpener().parent;
};

Service.refreshOpener = function(){
	var doc = Service.getOpener();
	doc.location.href = doc.location.href;
	window.close();
};

Service.refreshOpenerFrame = function(frameName){

	var frame = opener.document.getElementById(frameName);
	var src = frame.src;
	var win = frame.contentWindow;
	win.document.location.href = src;
	window.close();
};

Service.checkField = function(field){
	if(field.value == ''){
		AlertBox.alert('Proszę wypełnić wskazane pole.');
		field.select();
		return false;
	}
	else{
		return true;
	}
};

Service.activateField = function(field, isActive){
	field.disabled = !isActive;
	field.style.backgroundColor = isActive ? 'white' : '#ededed';
};

/**
 * Zwraca rozszerzenie pliku.
 */
Service.getSuffix = function(fileName){
	var fileName = fileName.toLowerCase();
	var pos = fileName.lastIndexOf('.');
	return fileName.substring(pos + 1);
};

/**
 * Wyswietla banery w formatach: swf, jpg, gif, png.
 */
Service.dispalyBanner = function(params){
	var suffix = Service.getSuffix(params['src']);
	if(suffix == 'swf'){
		DisplayFlash.init(params);
	}
	else{
		var img = '<img src="' + params['src'] + '" width="' +
		params['width'] + '" height="' + params['height'] + '" border="0" />';

		if(typeof(params['href']) != 'undefined' && params['href'] != ''){
			img = '<a href="' + params['href'] + '" target="' + params['target']
		+ '">' + img + '</a>';
		}

		document.write(img);
	}
};

Service.confirmDelete = function(href, text){
	if(typeof(text) == 'undefined'){
		var text = "Czy jesteś pewien?";
	}
	text += "\n<br/><br/>Jeżeli tak kliknij <b>OK</b>, jeżeli nie kliknij <b>Anuluj</b>.";

	AlertBox.confirm(text, {onComplete: function(e) {
		if(e) {
			document.location.href = href;
		}
	  }
	});
};

Service.confirmOpenWindow = function(href, text){
	AlertBox.confirm(text, {onComplete: function(e) {
		if(e) {
			window.open(href);
		}
	  }
	});
};

Service.checkPassword = function(field1, field2){
	if(field1.value != field2.value){
		AlertBox.error('Wprowadzone hasła muszą być identyczne');
		field1.value = field2.value = '';
		field1.focus();
		return false;
	}
	else{
		return true;
	}
};

Service.openSubEditor = function(href, params){
	var p = {
		width:	600,
		top:	150,
		left:	100
	};
	if(typeof(params) == 'object'){
		for(var i in params){
			p[i] = params[i];
		}
	}
	Service.openWindow(href, p);
};

Service.openWindow = function(href, params){
	var p = '';
	var name = '';
	if(typeof(params) == 'object'){
		for(var i in params){
			if(i == 'name')
				name = params[i];
			else
				p += i + '=' + params[i] + ',';
		}
	}
	window.open(href, name, p);
};


//---

Service.openResizer = function(key, value){
	var path = Config.getPathToCommon() + Config.file.RESIZER + '?' + key + '=' + value;
	var params = {
		name:	'newWindow',
		left:	100,
		top:	100,
		width:	100,
		height:	100
	};
	Service.openWindow(path, params);
};

Service.openViewer = function(key, value){
	var path = Config.getPathToCommon() + Config.file.VIEWER + '?' + key + '=' + value;
	var params = {
		name: 'newWindow'
	};
	Service.openWindow(path, params);
};

Service.openResource = function(pathFile){
	if(Service.isImage(pathFile))
		Service.openImage(pathFile);
	else
		Service.openFile(pathFile);
};


Service.checkImage = function(pathFile){
	if(!Service.isImage(pathFile)){
		AlertBox.error('Podany plik musi byc formatu: jpg, gif, png.');
		return false;
	}
	return true;
};


Service.isImage = function(pathFile){
	var suffix = Service.getSuffix(pathFile);
	return (suffix == 'jpg' || suffix == 'gif' || suffix == 'png');
};

Service.openImage = function(pathFile){
	Service.openResizer('file', pathFile);
};

Service.openImageById = function(id){
	Service.openResizer('id', id);
};

Service.openFile = function(pathFile){
	var path = Config.getPathToData() + pathFile;
	window.open(path);
};

Service.openFileById = function(id){
	Service.openViewer('id', id);
};

//---

Service.insertString = function(field, text){
	if(typeof(text) != 'string'){
		var text = 'http://';
	}

	if(field.value == ''){
		field.value = text;
		field.select();
		field.onblur = function(){
			if(field.value == text){
				field.value = '';
			}
		}
	}
};

Service.addFavorite = function(){
	var l = document.location;
	var link = l.protocol + '//' + l.hostname;
	var title = l.hostname;

	var t = document.getElementsByTagName('title');

	if(document.all){ // for IE
		if(t[0].innerText != ''){
			title = t[0].innerText;
		}
		window.external.AddFavorite(link, title);
	}
	else{
		var title = t[0].firstChild.nodeValue;
		window.sidebar.addPanel(title, link, '');
	}
};


Service.insertString = function(field, text){
	if(typeof(text) != 'string'){
		var text = 'http://';
	}

	if(field.value == ''){
		field.value = text;
		field.select();
		field.onblur = function(){
			if(field.value == text){
				field.value = '';
			}
		}
	}
};

