﻿/***********************************************************/
/*                    tinyTips Plugin                      */
/*                      Version: 1.0                       */
/*                      Mike Merritt                       */
/*                 Updated: Feb 4th, 2010                  */
/***********************************************************/

(function($){
	$.fn.tinyTips = function (display, supCont) {

		/* User settings
		**********************************/

		// Enter the markup for your tooltips here. The wrapping div must have a class of tinyTip and
		// it must have a div with the class "content" somewhere inside of it.
		var tipFrame = '<div class="tinyTip"><div class="content"><div class="load-ajax"></div></div><div class="bottom">&nbsp;</div></div>';

		// Speed of the animations in milliseconds - 1000 = 1 second.
		var animSpeed = 300;

		/***************************************************************************************************/
		/* End of user settings - Do not edit below this line unless you are trying to edit functionality. */
		/***************************************************************************************************/

		// Global tinyTip variable;
		var tinyTip;
		var tText;

		// When we hover over the element that we want the tooltip applied to

		//$(this).hover(function() {
		if(display == 'show') {
			// Inject the markup for the tooltip into the page and
			// set the tooltip global to the current markup and then hide it.
			var tipCont = '';
			$('body').append(tipFrame);
			tinyTip = $('div.tinyTip');
			tinyTip.hide();

			if (supCont !== '') {
				$('.tinyTip .content').html(tipCont);
			}

			// Offsets so that the tooltip is centered over the element it is being applied to but
			// raise it up above the element so it isn't covering it.
			var yOffset = 42;//tinyTip.height();
			var xOffset = (((tinyTip.width() + 45))) - ($(this).width()); //(((tinyTip.width() - 10) / 2)) - ($(this).width() / 2);

			// Grab the coordinates for the element with the tooltip and make a new copy
			// so that we can keep the original un-touched.
			var pos = $(this).offset();
			var nPos = pos;

			// Add the offsets to the tooltip position
			nPos.top = pos.top - yOffset;
			nPos.left = pos.left - xOffset;

			// Make sure that the tooltip has absolute positioning and a high z-index,
			// then place it at the correct spot and fade it in.
			tinyTip.css('position', 'absolute').css('z-index', '1000');
			tinyTip.css(nPos).show(0);

		}
		//, function() {

		else if(display == 'hide') {

			$(this).attr('title', tText);

			// Fade the tooltip out once the mouse moves away and then remove it from the DOM.
			$('div.tinyTip').hide(0, function() {
				$(this).remove();
			});

		}
		//);

	}

})(jQuery);