Import('myLib/native/String.js');
Import('myLib/text/Crypt.js');
/*
Import('other/ImageLayer.js');
Import('myLib/layers/LayerManager.js');
Import('myLib/html/DisplayFlash.js');
Import('myLib/form/CheckForm.js');
*/

PageService = {};

Import.clone(Service, PageService);

//--
PageService.$_ = function(id) {
 return document.getElementById(id);
}

//--
PageService.clearField = function(input) {
  input.value = (input.value == 'szukaj...')?'':input.value;
}

//--
PageService.setField = function(input) {
  input.value = (input.value == '')?'szukaj...':input.value;
}

//--
PageService.goToPage = function(url) {
	document.location.href = url;
}

//--
PageService.sendAdvancedSearchForm = function(form) {
	var keyword = String(form.allKeywords.value);
	keyword = keyword.cleanHtmlTags();
	keyword = keyword.replace(new RegExp(/\\/g),"");
	keyword = keyword.replace(new RegExp(/\//g),"");

	form.allKeywords.value = keyword;
	form.submit();
}

PageService.sendSearchForm = function(form) {
	var keyword = String(form.allKeywords.value);
	keyword = keyword.cleanHtmlTags();
	keyword = keyword.replace(new RegExp(/\\/g),"");
	keyword = keyword.replace(new RegExp(/\//g),"");

	form.allKeywords.value = keyword;
	form.submit();
}

//---
PageService.openPage = function(href, target){
	var href = href.replaceAll('@', '&');
	if(target == '_self')
		document.location.href = href;
	else
		window.open(href);
};

PageService.closeLayer = function(id) {
	LayerManager.close(id);
};

//---
PageService.changeSelects = function(isShow){
	if(Browser.type == Browser.browsers.IE && Browser.version < 7){
		var items = document.getElementsByTagName('select');
		for(var i = 0; i < items.length; i++){
			items[i].style.visibility = isShow ? 'visible' : 'hidden';
		}
	}
};

//---
PageService.openImageById = function(id, description){
	var path = Config.getPathToPage() + Config.file.VIEWER + '?id=' + id;
	ImageLayer.showOnce(path, description);
};

PageService.openImage = function(image, description){
	var path = Config.getPathToData() + image;
	ImageLayer.showOnce(path, description);
};

PageService.openAddedImageById = function(id){
	ImageLayer.showById(id);
};

PageService.setImage = function(id, image, description){
	var path = Config.getPathToData() + image;
	ImageLayer.setValues(id, path, description);
};

//---
PageService.openPrintVersion = function(url){
	var params = {
		left:		20,
		top:		20,
		width: 		640,
		height: 	550,
		scrollbars: 'yes',
		menubar: 'yes'
	};
	Service.openWindow(url, params);
};

PageService.openViewNewsletter = function(){
	var params = {
		left:		20,
		top:		20,
		width: 		610,
		height: 	650,
		scrollbars: 'yes',
		resizable:	'yes'
	};
	var path = Config.PATH + Config.dir.PAGE + Config.file.NEWSLETTER;
	Service.openWindow(path, params);
};

PageService.openFormNotifier = function(){
	var params = {
		left:		100,
		top:		100,
		width: 		520,
		height: 	300
	};
	var path = Config.PATH + Config.dir.PAGE +
	Config.file.NOTIFIER + document.location.search;
	Service.openWindow(path, params);
};

//---
PageService.queueMonitor = function(){
	var pattern = 'queue_{index}_2';
	MyAJAX.init('queueValues', null, function(obj){
		var items = eval(obj.responseText);
		for(var i = 0; i < items.length; i++){
			var id = pattern.replace('{index}', i);
			document.getElementById(id).innerHTML = items[i];
		}
	});
};
