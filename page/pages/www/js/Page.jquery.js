$(document).ready(function(){

	/*
	// Font size
	var fontMultiplier = 0;
	var textSelectors = 'p, li, li a, div, .slide_item_text';
	$('#font-small').on('click', function() {
		console.log('s');
		if (fontMultiplier >= -1) {
			$(textSelectors).each(function() {
				var newSize = (parseInt($(this).css('font-size')) - 2) + 'px';
				var newLineHeight = (parseInt($(this).css('line-height')) - 2) + 'px';
				console.log(newSize);
				$(this).css('font-size', newSize);
				//$(this).css('line-height', newLineHeight);
			});
			--fontMultiplier;
		}
	});
	$('#font-large').on('click', function() {
		if (fontMultiplier <= 4) {
			$(textSelectors).each(function() {
				var newSize = (parseInt($(this).css('font-size')) + 2) + 'px';
				var newLineHeight = (parseInt($(this).css('line-height')) + 2) + 'px';
				console.log(newSize);
				$(this).css('font-size', newSize);
				//$(this).css('line-height', newLineHeight);
			});
			++fontMultiplier;
		}
	});
	$('#font-normal').on('click', function() {
		if (fontMultiplier > 0) {
			$(textSelectors).each(function() {
				var newSize = (parseInt($(this).css('font-size')) - (2 * fontMultiplier)) + 'px';
				var newLineHeight = (parseInt($(this).css('line-height')) - (2 * fontMultiplier)) + 'px';
				$(this).css('font-size', newSize);
				//$(this).css('line-height', newLineHeight);
			});
		}
		if (fontMultiplier < 0) {
			$(textSelectors).each(function() {
				var newSize = (parseInt($(this).css('font-size')) + (-2 * fontMultiplier)) + 'px';
				var newLineHeight = (parseInt($(this).css('line-height')) + (-2 * fontMultiplier)) + 'px';
				$(this).css('font-size', newSize);
				//$(this).css('line-height', newLineHeight);
			});
		}
		fontMultiplier = 0;
	});
	*/
	
	
	// Main Menu
	var menuTouched = 0;
	$("div.main_menu_item").mouseenter(function() {
		$(this).children(".main_menu_over").css('display', 'block');
		$(this).children(".main_menu_drop").css('display', 'block');
	});
	$("#container").on('touchstart', function(e) {
		if (menuTouched == 1 && $(e.target).parents("div.main_menu_item").length == 0) {
			$("div.main_menu_item").each(function(i, el) {
				$(el).children(".main_menu_over").css('display', 'none');
				$(el).children(".main_menu_drop").css('display', 'none');
			});
			menuTouched = 0;
		}
	});
	$("div.main_menu_item").on('touchstart', function(e) {
		//e.preventDefault();
		if ($(this).children(".main_menu_over").length == 0 || $(this).children(".main_menu_drop").length == 0) {
			return true;
		}
		if ($(this).children(".main_menu_over").css('display') == 'block' && $(this).children(".main_menu_drop").css('display') == 'block') {
			return true;
		}
		$("div.main_menu_item").each(function(i, el) {
			$(el).children(".main_menu_over").css('display', 'none');
			$(el).children(".main_menu_drop").css('display', 'none');
		});
		$(this).children(".main_menu_over").css('display', 'block');
		$(this).children(".main_menu_drop").css('display', 'block');
		menuTouched = 1;
		return false;
	});
	$("div.main_menu_item").mouseleave(function() {
		$(this).children(".main_menu_over").css('display', 'none');
		$(this).children(".main_menu_drop").css('display', 'none');
	});

	// Slider
	if($('#slider').length>0){
		$('#slider').cycle({
			fx:      'fade',
			speed:	 850,
			timeout: 4000,
			//prev:    '.slidePrev',
			//next:    '.slideNext',
			pager:   '#slidePages',
			cleartypeNoBg:true
		});
		$('#slider_play').on('click', function (i, e) {
			$('#slider_play div').toggleClass('slider_paused');
			if ($('#slider_play div').hasClass('slider_paused')) {
				$('#slider').cycle('pause');
			} else {
				$('#slider').cycle('resume');
			}
		});
	};

	// Gallery Slider
	if($('#gallery_slider').length>0){
		$('#gallery_slider').cycle({
			fx:     'fade',
			speed:	 1500,
			timeout: 12000,
			//prev:    '.slidePrev',
			//next:    '.slideNext',
			pager:   '#gallery_slide_pages',
			cleartypeNoBg:true
		});
		$('#gallery_slider_play').on('click', function (i, e) {
			$('#gallery_slider_play div').toggleClass('slider_paused');
			if ($('#gallery_slider_play div').hasClass('slider_paused')) {
				$('#gallery_slider').cycle('pause');
			} else {
				$('#gallery_slider').cycle('resume');
			} 
		});
	}
	
	// Gallery Slider 2
	if($('#gallery_slider2').length>0){
		$('#gallery_slider2').cycle({
			fx:     'fade',
			speed:	 1500,
			timeout: 12000,
			//prev:    '.slidePrev',
			//next:    '.slideNext',
			pager:   '#gallery_slide_pages2',
			cleartypeNoBg:true
		});
		$('#gallery_slider_play2').on('click', function (i,e ) {
			$('#gallery_slider_play2 div').toggleClass('slider_paused');
			if ($('#gallery_slider_play2 div').hasClass('slider_paused')) {
				$('#gallery_slider2').cycle('pause');
			} else {
				$('#gallery_slider2').cycle('resume');
			}
		});
	}
	
	// Calendar events cloud loading 
	$("table.calendar a.calendarLnik").mouseenter(function() {
		$(this).tinyTips('show', '');
		var date = $(this).attr('rel');
		var lang = $(this).attr('lang');
		$.post("ajax.php", {
			 action: 'loadCalendarEvents',
			 args:[date, lang]
			},
			 function(data) {
				if(data != ''){
					$('.tinyTip .content').html(data);
				} else {
					$('.tinyTip .content').html('Brak wydarze�');
				}
			 }
		);
	});
	$("table.calendar a.calendarLnik").mouseleave(function() {
		$(this).tinyTips('hide');
	});
	
	if ($("#searcher_form").length>0) {
		$.datepicker.setDefaults($.datepicker.regional['pl']);
		$("#date_from").datepicker();
		$("#date_to").datepicker();
	}
	
});